﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Models.Enums
{
    public enum MatchResult
    {
        Win,
        Draw,
        Lose
    }
}
