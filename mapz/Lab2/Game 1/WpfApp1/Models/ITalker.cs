﻿using System;

namespace WpfApp1.Models
{
    public interface ITalker
    {
        void Talk();
    }
    public class GoodTalker : ITalker
    {
        void ITalker.Talk()
        {
            Console.WriteLine("Hello Nazar! Good luck!");
        }
    }

    public class EvilTalker : ITalker
    {
        void ITalker.Talk()
        {
            Console.WriteLine("My power will destroy you!");
        }
    }
}