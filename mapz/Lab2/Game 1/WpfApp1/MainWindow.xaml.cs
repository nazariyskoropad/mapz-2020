﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Models;
using WpfApp1.Models.Enums;
using WpfApp1.Services;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Caretaker _caretaker;
        private ConcreteMediator _mediator = new ConcreteMediator();
        private HorizontalPlayer horizontalPlayer;
        private VerticalPlayer verticalPlayer;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var shop1 = Shop_Lazy.Instance;
            var shop2 = Shop_Safe.Instance;

            Console.WriteLine(shop1.ToString() + "Hash code : " + shop1.GetHashCode());
            Console.WriteLine(shop2.ToString() + "Hash code : " + shop2.GetHashCode());

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //var facade = new Facade(new MatchMaker(new Footballer(), Level.Easy), new Models.Player());
            //facade.PlayMatch();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var builder = new HardEnemyBuilder(new Footballer());
            builder.CreateNewTalkingEnemy();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var builder = new HardEnemyBuilder(new Footballer());
            builder.CreateNewEnemy();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("MatchStarted");
            Console.BackgroundColor = ConsoleColor.Black;

            _caretaker = new Caretaker();
            ShowResult(_caretaker.GetCurrentResult());
        }

        private void ShowResult(GameState res)
        {
            Console.WriteLine($"Current result {res.GoalsScored} : {res.GoalsMissed}");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            _caretaker.Score();
            ShowResult(_caretaker.GetCurrentResult());
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            _caretaker.Miss();
            ShowResult(_caretaker.GetCurrentResult());
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            _caretaker.CreateSave();
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            _caretaker.LoadLastSave();
            ShowResult(_caretaker.GetCurrentResult());
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            var screenModeFlow = new ScreenModeFlow();
            screenModeFlow.Do();
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            var dbFlow = new Flow();
            dbFlow.Main();
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            horizontalPlayer.Move();
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            horizontalPlayer = new HorizontalPlayer(_mediator);
            verticalPlayer = new VerticalPlayer(_mediator);
            _mediator.Player1 = horizontalPlayer;
            _mediator.Player2 = verticalPlayer;
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            verticalPlayer.Move();
        }
    }
}
