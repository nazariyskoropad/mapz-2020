﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models.Enums;

namespace WpfApp1.Models
{
    public class Player : BaseEntity
    {
        public string NickName { get; set; }
        public string Password { get; set; }
        public DateTime RegistrationDate { get;}
        public int Coins { get; set; }
        public Status Status { get; set; }
        public List<Footballer> Footballers { get; set; }

        internal void EquipItems()
        {
            throw new NotImplementedException();
        }
    }
}
