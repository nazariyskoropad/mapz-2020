﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Models.Enums
{
    public enum Firm
    {
        Adidas,
        Nike,
        Puma,
        NewBalance
    }
}
