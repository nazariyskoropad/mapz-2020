﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models.Enums;
using WpfApp1.Models.TShirts;
using WpfApp1.Services;

namespace WpfApp1.Models
{
    public class Shop_Lazy
    {
        private static readonly Lazy<Shop_Lazy> lazy = new Lazy<Shop_Lazy>(() => new Shop_Lazy());
        public static Shop_Lazy Instance
        {
            get { return lazy.Value; }
        }
        private Shop_Lazy()
        {
            Console.WriteLine($"Shop_Lazy ctor : {DateTime.Now.TimeOfDay}");
        }
        public List<IBoost> Boosts { get; set; }
        public List<Boots.Boots> Boots { get; set; }
        public List<TShirts.TShirt> TShirts { get; set; }
    }

    public class Shop_Safe
    {
        private static object locker = new object();
        private static Shop_Safe instance = null;
        private IAbstractFactory _factory;
        public static Shop_Safe Instance
        {
            get
            {
                lock(locker)
                {
                    if (instance == null)
                        instance = new Shop_Safe();
                }
                return instance;
            }
        }
        private Shop_Safe()
        {
            Console.WriteLine($"Shop_safe ctor : {DateTime.Now.TimeOfDay}");
        }
        public List<IBoost> Boosts { get; set; }
        public List<Boots.Boots> Boots { get; set; }
        public List<TShirts.TShirt> TShirts { get; set; }

        public Boots.Boots CreateCustomBoots(Status status)
        {
            switch (status)
            {
                case Status.Normal:
                    _factory = new SimpleFactory();
                    break;
                case Status.Vip:
                    _factory = new VipFactory();
                    break;
            }

            return _factory.CreateBoots();
        }

        public void BuyTShirt(int shirtId, int price)
        {

        }
    }


}
