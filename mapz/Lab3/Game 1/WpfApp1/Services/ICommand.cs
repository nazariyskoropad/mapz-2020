﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Services
{
    public interface ICommand
    {
        public void Execute();
        public void Undo();
    }

    public class SaveCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Saving info to db");
        }

        public void Undo()
        {
            Console.WriteLine("Cancel saving info to db");
        }
    }

    public class DeleteCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Delete record from db");
        }

        public void Undo()
        {
            Console.WriteLine("Cancel delete from db");
        }
    }

    public class SelectCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Select record from db");
        }

        public void Undo()
        {
            Console.WriteLine("Selection was canceled");
        }
    }

    public class UpdateCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Update record to db");
        }

        public void Undo()
        {
            Console.WriteLine("Update was canceled");
        }
    }

    public class DbExecutor
    {
        ICommand _command;

        public void SetCommand(ICommand command)
        {
            _command = command;
        }

        public void Do()
        {
            _command.Execute();
        }
        public void Undo()
        {
            _command.Undo();
        }
    }

    public class Flow
    {
        public void Main()
        {
            var dbExecutor = new DbExecutor();
            dbExecutor.SetCommand(new DeleteCommand());
            dbExecutor.Do(); 
            dbExecutor.Undo();

            dbExecutor.SetCommand(new SaveCommand());
            dbExecutor.Do();
            dbExecutor.Undo();
        }
    }
}
