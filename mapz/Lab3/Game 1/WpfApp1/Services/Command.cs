﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Services
{
    public interface ICommand1
    {
        public void Execute();
        public void Undo();
    }


    // Receiver - Получатель
    class NightMode
    {
        public void On()
        {
            Console.WriteLine("Night mode on");
        }
        public void Off()
        {
            Console.WriteLine("Night mode off");
        }
    }

    class NeonMode
    {
        public void On()
        {
            Console.WriteLine("Neon mode on");
        }
        public void Off()
        {
            Console.WriteLine("Neon mode off");
        }
    }

    class NightModeOnCommand : ICommand1
    {
        NightMode _nightMode;
        public NightModeOnCommand(NightMode nightMode)
        {
            _nightMode = nightMode;
        }
        public void Execute()
        {
            _nightMode.On();
        }
        public void Undo()
        {
            _nightMode.Off();
        }
    }

    class NeonModeOnCommand : ICommand1
    {
        NeonMode _neonMode;
        public NeonModeOnCommand(NeonMode neonMode)
        {
            _neonMode = neonMode;
        }
        public void Execute()
        {
            _neonMode.On();
        }
        public void Undo()
        {
            _neonMode.Off();
        }
    }

    // Invoker - инициатор
    class ScreenModeChanger
    {
        ICommand1 command;

        public ScreenModeChanger() { }

        public void SetCommand(ICommand1 com)
        {
            command = com;
        }

        public void TurnOn()
        {
            command.Execute();
        }
        public void TurnOff()
        {
            command.Undo();
        }
    }

    public class ScreenModeFlow
    {
        public void Do()
        {

            var screenMode = new ScreenModeChanger();
            var nightMode = new NightMode();

            screenMode.SetCommand(new NightModeOnCommand(nightMode));
            screenMode.TurnOn();
            screenMode.TurnOff();

            screenMode.SetCommand(new NeonModeOnCommand(new NeonMode()));
            screenMode.TurnOn();
            screenMode.TurnOff();
        }
    }
}
