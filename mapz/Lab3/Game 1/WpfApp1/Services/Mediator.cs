﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Services
{
    public abstract class Mediator
    {
        public abstract void Send(Player colleague);
    }

    public abstract class Player
    {
        protected Mediator mediator;

        public Player(Mediator mediator)
        {
            this.mediator = mediator;
        }

        internal void EquipItems()
        {
            throw new NotImplementedException();
        }
    }

    class HorizontalPlayer : Player
    {
        public HorizontalPlayer(Mediator mediator)
            : base(mediator)
        { }

        public void Move()
        {
            mediator.Send(this);
        }

        public void Notify()
        {
            Console.WriteLine("Player1: Moving horizontally");
        }
    }

    class VerticalPlayer : Player
    {
        public VerticalPlayer(Mediator mediator)
            : base(mediator)
        { }

        public void Move()
        {
            mediator.Send(this);
        }

        public void Notify()
        {
            Console.WriteLine("Player2: Jumping");
        }
    }

    class ConcreteMediator : Mediator
    {
        public HorizontalPlayer Player1 { get; set; }
        public VerticalPlayer Player2 { get; set; }
        public override void Send(Player player)
        {
            if (Player1 == player)
                Player2.Notify();
            else
                Player1.Notify();
        }
    }
}
