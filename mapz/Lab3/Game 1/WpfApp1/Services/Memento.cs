﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace WpfApp1.Services
{
    [Serializable]
    public class GameState
    {
        public int GoalsScored { get; set; }
        public int GoalsMissed { get; set; }
        public GameState(int goalsScored, int goalsMissed)
        {
            GoalsScored = goalsScored;
            GoalsMissed = goalsMissed;
        }
    }

    [Serializable]
    public class Memento
    {
        [JsonProperty(PropertyName = "_state")]
        public readonly GameState _state;
        public Memento(GameState state)
        {
            _state = state;
        }
        public GameState GetState()
        {
            return _state;
        }
    }

    public class GameOriginator
    {
        public int GoalsScored { get; set; }
        public int GoalsMissed { get; set; }

        public GameState _state = new GameState(0, 0);
        public void Score()
        {
            Console.WriteLine("Goal scored");
            GoalsScored++;
            _state = new GameState(GoalsScored, GoalsMissed);
        }
        public void Miss()
        {
            Console.WriteLine("Goal missed");
            GoalsMissed++;
            _state = new GameState(GoalsScored, GoalsMissed);
        }

        public Memento GameSave()
        {
            return new Memento(_state);
        }
        public void LoadGame(Memento memento)
        {
            _state = memento.GetState();
            GoalsScored = _state.GoalsScored;
            GoalsMissed = _state.GoalsMissed;
        }

        internal GameState GetCurrentResult()
        {
            return new GameState(GoalsScored, GoalsMissed);
        }
    }

    public class Caretaker
    {
        private readonly GameOriginator _game = new GameOriginator();
        //private readonly Stack<Memento> _quickSaves = new Stack<Memento>();
        private readonly string _quickSavesPath = @"D:\1univer\Semester_4\MAPZ\Game 1\Game 1\gameRes.json";
         
        public void Score()
        {
            _game.Score();
        }
        public void Miss()
        {
            _game.Miss();
        }

        public void CreateSave()
        {
            File.WriteAllText(_quickSavesPath, string.Empty);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Saving current result {_game.GoalsScored} : {_game.GoalsMissed}");

            var currentState = _game.GameSave();
            string jsonString = JsonConvert.SerializeObject(currentState, Formatting.Indented);

            File.WriteAllText(_quickSavesPath, jsonString);

            //_quickSaves.Push(_game.GameSave());
            Console.ForegroundColor = ConsoleColor.White;

        }
        public void LoadLastSave()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Loading last save");

            string jsonString = File.ReadAllText(_quickSavesPath);
            var gameToLoad = JsonConvert.DeserializeObject<Memento>(jsonString);
            _game.LoadGame(gameToLoad);

            //_game.LoadGame(_quickSaves.Peek());
            Console.ForegroundColor = ConsoleColor.White;
        }

        public GameState GetCurrentResult()
        {
            return _game.GetCurrentResult();
        }
    }
}
