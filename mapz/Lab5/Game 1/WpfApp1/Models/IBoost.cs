﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models.Enums;

namespace WpfApp1.Models
{
    public interface IBoost
    {
        public int Price { get; }
        public Status Status { get; }
        public string Name { get;}
        public void UseBoost();
    }
}
