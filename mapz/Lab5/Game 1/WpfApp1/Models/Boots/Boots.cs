﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models.Enums;

namespace WpfApp1.Models.Boots
{
    public abstract class Boots
    {
        public int Price { get; }
        public Status Status { get; }
        public string Name { get; }
        public string Color { get; }
    }
}
