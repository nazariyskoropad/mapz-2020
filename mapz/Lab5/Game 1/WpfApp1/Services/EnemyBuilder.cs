﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Constants;
using WpfApp1.Models;
using WpfApp1.Models.Enums;

namespace WpfApp1.Services
{
    public abstract class EnemyBuilder
    {
        protected Footballer _footballer;
        protected Random _random;
        protected Level level;
        public Enemy Enemy { get;  set; }
        public void CreateNewEnemy()
        {
            this.Enemy = new Enemy();
            Console.WriteLine("no talking enemy");
            Enemy.MakeShot();
        }

        public void CreateNewTalkingEnemy()
        {
            var number = (new Random()).Next(-10, 10);
            if(number < 0)
            {
                this.Enemy = new TalkingEnemy(new Enemy(), new EvilTalker());
                Console.WriteLine("Evil talking enemy");
            }
            if (number > 0)
            {
                this.Enemy = new TalkingEnemy(new Enemy(), new GoodTalker());
                Console.WriteLine("Good talking enemy");
            }
            Enemy.MakeShot();
        }

        public Enemy GetEnemy()
        {
            return Enemy;
        }
        public abstract void SetSpeed();
        public abstract void SetShot();
        public abstract void SetPass();
        public abstract void SetStrength();
    }

    public class HardEnemyBuilder : EnemyBuilder
    {
        public HardEnemyBuilder(Footballer footballer)
        {
            _footballer = footballer;
            _random = new Random();
            level = Level.Hard;
        }
        public override void SetPass()
        {
            Enemy.Pass = _footballer.Pass + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetShot()
        {
            Enemy.Shot = _footballer.Shot + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetSpeed()
        {
            Enemy.Speed = _footballer.Speed + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetStrength()
        {
            Enemy.Strength = _footballer.Strength + EnemyConstants.GetValueToAdd(level);
        }
    }

    public class MediumEnemyBuilder : EnemyBuilder
    {
        public MediumEnemyBuilder(Footballer footballer)
        {
            _footballer = footballer;
            _random = new Random();
            level = Level.Medium;
        }
        public override void SetPass()
        {
            Enemy.Pass = _footballer.Pass + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetShot()
        {
            Enemy.Shot = _footballer.Shot + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetSpeed()
        {
            Enemy.Speed = _footballer.Speed + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetStrength()
        {
            Enemy.Strength = _footballer.Strength + EnemyConstants.GetValueToAdd(level);
        }
    }

    public class EasyEnemyBuilder : EnemyBuilder
    {
        public EasyEnemyBuilder(Footballer footballer)
        {
            _footballer = footballer;
            _random = new Random();
            level = Level.Easy;
        }
        public override void SetPass()
        {
            Enemy.Pass = _footballer.Pass + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetShot()
        {
            Enemy.Shot = _footballer.Shot + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetSpeed()
        {
            Enemy.Speed = _footballer.Speed + EnemyConstants.GetValueToAdd(level);
        }

        public override void SetStrength()
        {
            Enemy.Strength = _footballer.Strength + EnemyConstants.GetValueToAdd(level);
        }
    }
}
