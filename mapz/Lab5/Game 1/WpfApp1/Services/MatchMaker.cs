﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models;
using WpfApp1.Models.Enums;

namespace WpfApp1.Services
{
    public class MatchMaker
    {
        private readonly Footballer _footballer;
        private readonly Level _level;
        private readonly EnemyCreator _enemyCreator;
        private readonly Enemy _enemy;
        private readonly EasyEnemyBuilder easyEnemyBuilder;
        private readonly MediumEnemyBuilder mediumEnemyBuilder;

        internal void SetupArea()
        {
            throw new NotImplementedException();
        }

        private readonly HardEnemyBuilder hardEnemyBuilder;

        public MatchMaker(Footballer footballer, Level level)
        {
            _footballer = footballer;
            _level = level;
            _enemyCreator = new EnemyCreator();
            _enemy = CreateEnemy(level);
            easyEnemyBuilder = new EasyEnemyBuilder(footballer);
            mediumEnemyBuilder = new MediumEnemyBuilder(footballer);
            hardEnemyBuilder = new HardEnemyBuilder(footballer);

        }

        internal void CreateEnemy()
        {
            throw new NotImplementedException();
        }

        private Enemy CreateEnemy(Level level)
        {
            switch(level)
            {
                case Level.Easy:
                    _enemyCreator.SetEnemyBuilder(easyEnemyBuilder);
                    break;
                case Level.Medium:
                    _enemyCreator.SetEnemyBuilder(mediumEnemyBuilder);
                    break;
                case Level.Hard:
                    _enemyCreator.SetEnemyBuilder(hardEnemyBuilder);
                    break;
            }
            _enemyCreator.ConstructEnemy();
            return _enemyCreator.GetEnemy();
        }

        public void PlayMatch()
        {
            throw new NotImplementedException();
        }
    }
}
