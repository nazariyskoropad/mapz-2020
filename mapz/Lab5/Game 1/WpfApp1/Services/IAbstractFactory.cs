﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models;
using WpfApp1.Models.Boots;
using WpfApp1.Models.TShirts;

namespace WpfApp1.Services
{
    public interface IAbstractFactory
    {
        Boots CreateBoots();
        TShirt CreateTShitrs();
    }

    public class SimpleFactory : IAbstractFactory
    {
        public Boots CreateBoots()
        {
            return new SimpleBoots();
        }

        public TShirt CreateTShitrs()
        {
            return new SimpleTShirt();
        }
    }

    public class VipFactory : IAbstractFactory
    {
        public Boots CreateBoots()
        {
            return new VipBoots();
        }

        public TShirt CreateTShitrs()
        {
            return new VipTShirt();
        }
    }
}
