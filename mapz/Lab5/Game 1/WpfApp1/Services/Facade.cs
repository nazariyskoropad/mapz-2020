﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models;

namespace WpfApp1.Services
{
    public class GameplayFacade
    {
        MatchMaker _matchMaker;
        private Models.Player _player;

        public GameplayFacade(MatchMaker matchMaker,  Models.Player player)
        {
            _matchMaker = matchMaker;
            _player = player;
        }
        //інкупсульовано складну логіку підсистеми (матч-мейкер + гравець)
        public void PlayMatch()
        {
            _player.EquipItems();
            _matchMaker.SetupArea();
            _matchMaker.CreateEnemy();
            _matchMaker.PlayMatch();
        }
    }
}
