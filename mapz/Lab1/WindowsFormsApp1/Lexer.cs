﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Lexer
    {
        private string _code;
        private char[] chars;
        public List<Token> tokens = new List<Token>();
        public Lexer(string code)
        {
            _code = code;
            chars = _code.ToCharArray();
        }


        private void Tokenize()
        {
            for (int i = 0; i < _code.Length;)
            {
                var ch = _code[i];
                if (ch == ' ')
                {
                    i++;
                }
                else if (ch == '=' || ch == '>' || ch == '<' || ch == '!')
                {
                    if (_code[i + 1] == '=')
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  2),
                            tokenType = TokenType.logical_oper
                        };
                        tokens.Add(newToken);
                        i++;
                    }
                    else if (ch == '=')
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  1),
                            tokenType = TokenType.oper
                        };
                        tokens.Add(newToken);
                    }
                    else
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  1),
                            tokenType = TokenType.logical_oper
                        };
                        tokens.Add(newToken);

                    }
                    i++;
                }
                else if (ch == '+')
                {
                    if (chars[i + 1] == '+')
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  2),
                            tokenType = TokenType.oper
                        };
                        tokens.Add(newToken);
                        i++;
                    }
                    else
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  1),
                            tokenType = TokenType.oper
                        };
                        tokens.Add(newToken);
                    }
                    i++;
                }
                else if (ch == '-')
                {
                    if (chars[i + 1] == '-')
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i,  2),
                            tokenType = TokenType.oper
                        };
                        tokens.Add(newToken);
                        i++;
                    }
                    else
                    {
                        var newToken = new Token
                        {
                            Value = new String(chars, i, 1),
                            tokenType = TokenType.oper
                        };
                        tokens.Add(newToken);
                    }
                    i++;
                }
                else if (ch == '*' || ch == '/')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.oper
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == ';')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.semicolon
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == ':')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i, 1),
                        tokenType = TokenType.colon
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == '(')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.lparent
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == ')')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.rparent
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == '{')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.lbrace
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (ch == '}')
                {
                    var newToken = new Token
                    {
                        Value = new String(chars, i,  1),
                        tokenType = TokenType.rbrace
                    };
                    tokens.Add(newToken);
                    i++;
                }
                else if (Char.IsLetter(ch))
                {
                    int j = 0;
                    do
                    {
                        j++;
                        if (i + j >= _code.Length)
                            throw new Exception("Expected semicolon at the end of statement");
                    } while (Char.IsLetterOrDigit(chars[i + j]));

                    var tokenValue = new String(chars, i, j);
                    var type = TokenType.varname;
                    if (IsKeyword(tokenValue))
                        type = TokenType.keyword;
                    else if (IsVartype(tokenValue))
                        type = TokenType.vartype;
                    var newToken = new Token
                    {
                        Value = tokenValue,
                        tokenType = type
                    };
                    tokens.Add(newToken);
                    i += j;
                }
                else if (Char.IsDigit(ch))
                {
                    var j = 1;
                    while (Char.IsDigit(chars[i + j]))
                        j++;

                    var newToken = new Token
                    {
                        Value = new String(chars, i, j),
                        tokenType = TokenType.constant
                    };
                    tokens.Add(newToken);
                    i += j;
                }
                else if (ch == '\"')
                {
                    var j = 1;
                    while (chars[i + j] != '\"')
                        j++;
                    var newToken = new Token
                    {
                        Value = new String(chars, i, j + 1),
                        tokenType = TokenType.constant
                    };
                    tokens.Add(newToken);
                    i += j + 1;
                }
                else i++;
            }
        }

        public List<Token> Execute()
        {
            Tokenize();
            return tokens;
        }

        private bool IsKeyword(string tokenValue)
        {
            var keywords = new List<string>() { "for", "if", "else", "elif", "while",
            "find", "in", "print"};
            bool isKeyword = false;
            foreach (var keyword in keywords)
            {
                if (tokenValue == keyword)
                {
                    isKeyword = true;
                    break;
                }
            }
            return isKeyword;
        }
        private bool IsVartype(string tokenValue)
        {
            var varTypes = new List<string>() { "int", "string"};
            bool isVarType = false;
            foreach (var varType in varTypes)
            {
                if (tokenValue == varType)
                {
                    isVarType = true;
                    break;
                }
            }
            return isVarType;
        }
    }
}
