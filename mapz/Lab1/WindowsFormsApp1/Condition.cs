﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Condition
    {
        private readonly List<Token> _tokens;

        public Condition(List<Token> tokens)
        {
            _tokens = tokens;
        }

        public bool Evaluate(Dictionary<string, string> variables)
        {
            //if(a) || if(1)
            if(_tokens.Count == 1)
            {
                var value = TryGetIntValue(_tokens[0], variables);
                return value != 0;
            }
            //if(!a) || if(!0)
            if (_tokens.Count == 2)
            {
                if (_tokens[0].Value != "!")
                    throw new Exception("Wrong condition. '!' expected");
                var value = TryGetIntValue(_tokens[1], variables);
                return !(value != 0);
            }
            //if (a > b)
            if(_tokens.Count == 3)
            {
                if (_tokens[1].tokenType != TokenType.logical_oper)
                    throw new Exception("Wrong condition. Expected logical operator!");

                var logicalOperator = _tokens[1].Value;
                var leftValue = TryGetIntValue(_tokens[0], variables);
                var rightValue = TryGetIntValue(_tokens[2], variables);

                switch (logicalOperator)
                {
                    case ">":
                        {
                            return leftValue > rightValue;
                        }
                    case ">=":
                        {
                            return leftValue >= rightValue;
                        }
                    case "<":
                        {
                            return leftValue < rightValue;
                        }
                    case "<=":
                        {
                            return leftValue <= rightValue;
                        }
                    case "==":
                        {
                            return leftValue == rightValue;
                        }
                    case "!=":
                        {
                            return leftValue != rightValue;
                        }
                    default:
                        {
                            throw new Exception("Wrong condition. Wrong logical operator");
                            break;  
                        }
                }
            }
            //if()
            else
                throw new Exception("Wrong condition");
        }

        private int TryGetIntValue(Token token, Dictionary<string, string> variables)
        {
            if (token.tokenType == TokenType.constant)
            {
                if (!IsDigit(token.Value))
                    throw new Exception("Wrong condition. Expected a number");
                else
                    return int.Parse(token.Value);
            }
            else if (token.tokenType == TokenType.varname)
            {
                var variableToken = variables.SingleOrDefault(v => v.Key == token.Value);
                if (variableToken.Equals(default(KeyValuePair<string, string>)))
                    throw new Exception("Wrong condition. Variable doesnt exist!");
                else
                {
                    if(!IsDigit(variableToken.Value))
                        throw new Exception("Wrong condition. Expected a number");
                    return int.Parse(variableToken.Value);
                }
            }
            else 
                throw new Exception("Wrong condition. Wrong type of values");
        }

        private bool IsDigit(string str)
        {
            if (str[0] != '-' && !Char.IsDigit(str[0]))
                return false;
            else
                foreach (var ch in str)
                {
                    if (!Char.IsDigit(ch))
                        return false;
                }
            return true;
        }
    }
}
