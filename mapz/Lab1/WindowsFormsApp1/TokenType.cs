﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public enum TokenType
    {
        lparent, rparent, lbrace, rbrace, keyword, colon,
        vartype, varname, constant, oper, logical_oper, semicolon
    }
}
