﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Token
    {
        public string Value { get; set; }
        public TokenType tokenType { get; set; }
    }
}
