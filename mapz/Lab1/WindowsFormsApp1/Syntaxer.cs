﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Syntaxer
    {
        private List<Token> _tokens;
        public Syntaxer(List<Token> tokens)
        {
            _tokens = tokens;
        }

        private bool CheckBraces()
        {
            if (_tokens.Count(t => t.tokenType == TokenType.lbrace) == _tokens.Count(t => t.tokenType == TokenType.rbrace))
                return true;
            return false; 
        }
        private bool CheckParatheses()
        {
            if (_tokens.Count(t => t.tokenType == TokenType.lparent) == _tokens.Count(t => t.tokenType == TokenType.rparent))
                return true;
            return false;
        }

        public void Execute(Dictionary<string, string> variables, List<IStatement> statements)
        {
            if (!CheckBraces()) throw new Exception("Check braces parity");
            if (!CheckParatheses()) throw new Exception("Check paratheses parity");

            for (int i = 0; i < _tokens.Count;)
            {
                if (_tokens[i].tokenType == TokenType.varname)
                {
                    int j = i + 1;
                    while (j < _tokens.Count && _tokens[j].tokenType != TokenType.semicolon)
                        j++;

                    if (j == _tokens.Count)
                        throw new Exception("Semicolon is missed");

                    statements.Add(new Assign(new List<Token>(_tokens.GetRange(i, j - i + 1))));

                    i = j;
                }

                else if (_tokens[i].Value == "print")
                {
                    int j = i + 1;
                    while (j < _tokens.Count && _tokens[j].tokenType != TokenType.semicolon)
                        j++;
                    if (j == _tokens.Count)
                        throw new Exception("Expected semicolon at the end of statement");
                    statements.Add(new Print(new List<Token>(_tokens.GetRange(i, j - i + 1))));
                    i = j;
                }

                else if (_tokens[i].Value == "if")
                {
                    i = HandleIf(i, variables, statements);
                }

                else if (_tokens[i].Value == "while")
                {
                    i = HandleWhileLoop(i, variables, statements);
                }

                else if (_tokens[i].Value == "find")
                {
                    int j = i + 1;
                    while (j < _tokens.Count && _tokens[j].tokenType != TokenType.semicolon)
                        ++j;
                    if (j == _tokens.Count)
                        throw new Exception("Expected semicolon in search operation!");
                    statements.Add(new Find(new List<Token>(_tokens.GetRange(i, j - i + 1))));
                    i = j;
                }

                else i++;
            }


        }

        private int HandleWhileLoop(int whilePosition, Dictionary<string, string> variables, List<IStatement> statements)
        {            
            //condition
            int conditionLparent = whilePosition + 1;
            int conditionRparent = whilePosition + 1;

            while (conditionRparent < _tokens.Count && _tokens[conditionRparent].tokenType != TokenType.rparent)
                ++conditionRparent;
            if (conditionLparent >= _tokens.Count || _tokens[conditionLparent].tokenType != TokenType.lparent ||
                conditionRparent >= _tokens.Count)
                throw new Exception("SYNTAX ERROR: EXPECTED CONDITION AFTER IF BRANCH");

            //body
            int bodyLbrace = conditionRparent + 1;
            int bodyRbrace = conditionRparent + 1;        
            int brace_counter = 0;
            while (bodyRbrace < _tokens.Count)
            {
                if (_tokens[bodyRbrace].tokenType == TokenType.lbrace)
                    ++brace_counter;
                else if (_tokens[bodyRbrace].tokenType == TokenType.rbrace)
                    --brace_counter;
                if (brace_counter == 0)
                    break;
                ++bodyRbrace;
            }

            var whileStatements = new List<IStatement>();

            var whileSyntaxer = new Syntaxer(new List<Token>(_tokens.GetRange(bodyLbrace + 1, bodyRbrace - bodyLbrace - 1)));
            whileSyntaxer.Execute(variables, whileStatements);

            statements.Add(new WhileLoop( new List<Token>(_tokens.GetRange(conditionLparent + 1, conditionRparent - conditionLparent - 1))
                , whileStatements));

            return bodyRbrace;

        }

        private int HandleIf(int ifPosition, Dictionary<string, string> variables, List<IStatement> statements)
        {
            //condition
            int conditionLparent = ifPosition + 1;
            int conditionRparent = ifPosition + 1;
            while (conditionRparent < _tokens.Count && _tokens[conditionRparent].tokenType != TokenType.rparent)
                ++conditionRparent;
            if (conditionLparent >= _tokens.Count || _tokens[conditionLparent].tokenType != TokenType.lparent ||
                conditionRparent >= _tokens.Count)
                throw new Exception("SYNTAX ERROR: EXPECTED CONDITION AFTER IF BRANCH");

            //body
            int bodyLbrace = conditionRparent + 1;
            int bodyRbrace = conditionRparent + 1;        //TO FIRST TOKEN AFTER LBRACE
            int brace_counter = 0;
            while (bodyRbrace < _tokens.Count)
            {
                if (_tokens[bodyRbrace].tokenType == TokenType.lbrace)
                    ++brace_counter;
                else if (_tokens[bodyRbrace].tokenType == TokenType.rbrace)
                    --brace_counter;
                if (brace_counter == 0)
                    break;
                ++bodyRbrace;
            }

            if (bodyLbrace >= _tokens.Count || _tokens[bodyLbrace].tokenType != TokenType.lbrace
                    || bodyRbrace >= _tokens.Count)
                throw new Exception("SYNTAX ERROR: EXPECTED CLOSING BRACE");

            var ifStatements = new List<IStatement>();
            var elseStatements = new List<IStatement>();

            //if
            var ifSyntaxer = new Syntaxer(new List<Token>(_tokens.GetRange(bodyLbrace + 1, bodyRbrace - bodyLbrace - 1)));
            ifSyntaxer.Execute(variables, ifStatements);

            int elseIndex = bodyRbrace + 1;
            int elseLbrace = elseIndex + 1;
            int elseRbrace = elseIndex + 1;

            if (elseIndex < _tokens.Count && _tokens[elseIndex].Value == "else")
            {
                brace_counter = 0;
                while (elseRbrace < _tokens.Count)
                {
                    if (_tokens[elseRbrace].tokenType == TokenType.lbrace)
                        ++brace_counter;
                    else if (_tokens[elseRbrace].tokenType == TokenType.rbrace)
                        --brace_counter;
                    if (brace_counter == 0)
                        break;
                    ++elseRbrace;
                }

                if (elseLbrace >= _tokens.Count || _tokens[elseLbrace].tokenType != TokenType.lbrace
                    || elseRbrace >= _tokens.Count)
                    throw new Exception("Closing brace for else expected!");

                // CREATE STATEMENTS LIST FOR ELSE BODY
                var elseSyntaxer = new Syntaxer(new List<Token>(_tokens.GetRange(elseLbrace + 1, elseRbrace - elseLbrace - 1)));
                elseSyntaxer.Execute(variables, elseStatements);

                ++elseRbrace;
            }
            else
                elseRbrace -= 2;

            //CREATE STATEMENT FOR GENERAL IF BRANCH
            statements.Add(new IfBranch(new List<Token>(_tokens.GetRange(conditionLparent + 1, conditionRparent - conditionLparent - 1)),
                ifStatements, elseStatements));
                
            return elseRbrace;
        }
    }
}
