﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public interface IStatement
    {
        List<Token> _tokens { get; }
        TreeView treeView { get; set; }
        void Execute(Dictionary<string, string> variables);
        //  void BuildTree();
    }

    public class Assign : IStatement
    {
        public List<Token> _tokens { get; }
        public TreeView treeView { get; set; }
        public Assign(List<Token> tokens)
        {
            _tokens = tokens;
        }

        public void Execute(Dictionary<string, string> variables)
        {
            if (_tokens[0].tokenType != TokenType.varname)
                throw new Exception("Wrong first token in assignment");
            if (_tokens[1].Value != "=")
                throw new Exception("Variable must be followed by '='");
            if (_tokens[2].tokenType == TokenType.semicolon)
                throw new Exception("After '=' there must be an expression!");

            int i = 2;
            string tree;

            while (_tokens[i].tokenType != TokenType.semicolon)
            {
                if (_tokens[i].tokenType != TokenType.constant
                    && _tokens[i].tokenType != TokenType.varname
                    && _tokens[i].tokenType != TokenType.oper
                    && _tokens[i].tokenType != TokenType.lparent
                    && _tokens[i].tokenType != TokenType.rparent)
                {
                    throw new Exception("Invalid token in assignment!");
                }
                ++i;
            }

            var expression = new Expression(new List<Token>(_tokens.GetRange(2, i - 2)));
            var result = expression.Evaluate(variables);
            BuildTree(expression._tokens);

            var variableToken = variables.SingleOrDefault(v => v.Key == _tokens[0].Value);
            if (variableToken.Equals(default(KeyValuePair<string, string>)))
                variables.Add(_tokens[0].Value, result);
            else
                variables[_tokens[0].Value] = result;
        }

        void BuildTree(List<Token> tokens)
        {
            tokens.Reverse();
            //       tokens.Add(_tokens[0]);
            //       tokens.Add(_tokens[1]);
            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }

            TreeNode treeNodeAssign = new TreeNode();
            treeNodeAssign.Text = _tokens[1].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeAssign);
            treeView.SelectedNode = treeNodeAssign;

            TreeNode treeNodeVariable = new TreeNode();
            treeNodeVariable.Text = _tokens[0].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeVariable);


            TreeNode treeNodeFirst = new TreeNode();
            treeNodeFirst.Text = tokens[0].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeFirst);
            treeView.SelectedNode = treeNodeFirst;

            Token prevToken = null;
            Token nextToken = null;
            for (int i = 1; i < tokens.Count; i++)
            {
                var token = tokens[i];

                TreeNode treeNode = new TreeNode();
                treeNode.Text = token.Value;
                int val;

                if (treeView.SelectedNode.Nodes.Count >= 2 || (int.TryParse(treeView.SelectedNode.Text, out val)))
                {
                    while (treeView.SelectedNode.Nodes.Count >= 2 || (int.TryParse(treeView.SelectedNode.Text, out val)))
                        treeView.SelectedNode = treeView.SelectedNode.Parent;
                }
                treeView.SelectedNode.Nodes.Add(treeNode);
                treeView.SelectedNode = treeNode;
            }
            treeView.SelectedNode = treeView.SelectedNode.Parent;
        }
    }

    public class Print : IStatement
    {
        string Path { get; set; } = "D:\\1univer\\Semester_4\\MAPZ\\Interpretator\\output.txt";

        public List<Token> _tokens { get; }
        public TreeView treeView { get; set; }


        public Print(List<Token> tokens)
        {
            _tokens = tokens;
        }
        public void Execute(Dictionary<string, string> variables)
        {
            if (_tokens[0].Value != "print")
                throw new Exception("Invalid print statement");
            if (_tokens[1].Value == ";")
                throw new Exception("Nothing to print");
            int i = 1;
            while (_tokens[i].tokenType != TokenType.semicolon)
            {
                if (_tokens[i].tokenType != TokenType.constant
                    && _tokens[i].tokenType != TokenType.varname
                    && _tokens[i].tokenType != TokenType.oper
                    && _tokens[i].tokenType != TokenType.lparent
                    && _tokens[i].tokenType != TokenType.rparent)
                {
                    throw new Exception("Invalid token in assignment!");
                }
                ++i;
            }
            var expression = new Expression(new List<Token>(_tokens.GetRange(1, i - 1)));
            string result = expression.Evaluate(variables);
            BuildTree(expression._tokens);

            while (true)
            {
                var lapki = result.IndexOf('\"');
                if (result[0] == '\"' || lapki != -1)
                    result = result.Remove(lapki, 1);
                else
                    break;
            }

            using (var streamWriter = new StreamWriter(Path, true))
            {
                streamWriter.WriteLine(result);
            }
        }

        void BuildTree(List<Token> tokens)
        {
            tokens.Reverse();
            while (true)
            {
                var node = treeView.SelectedNode;
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else" || treeView.SelectedNode.Text == "while")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            TreeNode treeNodeVariable = new TreeNode();
            treeNodeVariable.Text = _tokens[0].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeVariable);
            treeView.SelectedNode = treeNodeVariable;

            TreeNode treeNodeFirst = new TreeNode();
            treeNodeFirst.Text = tokens[0].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeFirst);
            treeView.SelectedNode = treeNodeFirst;

            for (int i = 1; i < tokens.Count; i++)
            {
                var token = tokens[i];

                TreeNode treeNode = new TreeNode();
                treeNode.Text = token.Value;
                int val;

                if (treeView.SelectedNode.Nodes.Count >= 2 || (int.TryParse(treeView.SelectedNode.Text, out val)))
                {
                    while (treeView.SelectedNode.Nodes.Count >= 2 || (int.TryParse(treeView.SelectedNode.Text, out val)))
                        treeView.SelectedNode = treeView.SelectedNode.Parent;
                }
                treeView.SelectedNode.Nodes.Add(treeNode);
                treeView.SelectedNode = treeView.SelectedNode.Parent.Parent;
            }
        }
    }
    public class Find : IStatement
    {
        string Path { get; set; } = "D:\\1univer\\Semester_4\\MAPZ\\Interpretator\\output.txt";

        public List<Token> _tokens { get; }
        public TreeView treeView { get; set; }

        bool success = false;
        public Find(List<Token> tokens)
        {
            _tokens = tokens;
        }
        public void Execute(Dictionary<string, string> variables)
        {
            if (_tokens[0].Value != "find" || _tokens[2].Value != "in")
                throw new Exception("Invalid find statement!");
            if ((_tokens[1].tokenType != TokenType.constant && _tokens[1].tokenType != TokenType.varname)
                || (_tokens[3].tokenType != TokenType.constant && _tokens[3].tokenType != TokenType.varname))
                throw new Exception("Invalid find statement arguments!");

            string expr;
            string source;

            if (_tokens[1].tokenType == TokenType.varname)
            {
                var variableToken = variables.SingleOrDefault(v => v.Key == _tokens[1].Value);
                if (variableToken.Equals(default(KeyValuePair<string, string>)))
                    throw new Exception("Wrong condition. Variable doesnt exist!");
                else
                {
                    expr = variableToken.Value;
                }
            }
            else
            {
                expr = _tokens[1].Value;
            }

            if (_tokens[3].tokenType == TokenType.varname)
            {
                var variableToken = variables.SingleOrDefault(v => v.Key == _tokens[3].Value);
                if (variableToken.Equals(default(KeyValuePair<string, string>)))
                    throw new Exception("Wrong condition. Variable doesnt exist!");
                else
                {
                    source = variableToken.Value;
                }
            }
            else
            {
                source = _tokens[3].Value;
            }


            while (true)
            {
                var lapki = source.IndexOf('\"');
                if (source[0] == '\"' || lapki != -1)
                    source = source.Remove(lapki, 1);
                else
                    break;
            }


            var result = HandleExpression(expr, source);


            using (var streamWriter = new StreamWriter(Path, true))
            {
                if (result.Count != 0)
                {
                    success = true;
                    foreach (var res in result)
                    {
                        streamWriter.WriteLine($"Found {res.Value} at position {res.Key}");
                    }
                }
                else
                    streamWriter.WriteLine($"Couldnt find any matches");
            }


            
            BuildTree(_tokens, source, expr);
        }


        private Dictionary<int, string> HandleExpression(string expr, string source)
        {
            var arr = expr.ToCharArray();
            var allAscii1 = Enumerable.Range('\x1', 127).ToArray();
            var allAscii = new List<char>();

            var output = new Dictionary<int, string>();

            var allWords = source.Split(new char[]{ ' ', '.'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in allAscii1)
            {
                allAscii.Add((char)item);
            }
            List<string> result = new List<string>();
            string res = "";

            for(int i = 0; i < arr.Length - 1; i++)
            {
                var ch = arr[i];
                if (Char.IsLetterOrDigit(ch))
                    res += ch.ToString();
                else if(ch == '-')
                {
                    res = res.Remove(res.Length - 1);
                    if(Char.IsLetterOrDigit(arr[i-1]) && Char.IsLetterOrDigit(arr[i - 1]))
                    {
                        var leftChar = arr[i - 1];
                        var rightChar = arr[i + 1];
                        if(arr[i - 2] == '[' && arr[i+2] == ']')
                        {
                            var range = allAscii.GetRange(leftChar, rightChar - leftChar);
                            foreach(var char1 in range)
                            {
                                result.Add(res + char1);
                            }
                            res += "_";
                        }
                        i += 2;
                    }
                }
                else if(ch == '#')
                {
                    res += "#";
                    if (result.Count != 0)
                        for (int j = 0; j < result.Count; j++)
                            result[j] = result[j] + "#";
                }
                else if(ch == '.')
                {
                    res += ".";
                    if (result.Count != 0)
                        for (int j = 0; j < result.Count; j++)
                            result[j] = result[j] + ".";
                }
            }
            result.Add(res);

            foreach (var item in result)
            {

                if (item.Contains("."))
                {
                    var originalSource = source;
                    var differencesSum = 0;
                    while (true)
                    {
                        var subItems = item.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                        var indexes = new List<int>();

                        bool matches = true;
                        foreach (var subItem in subItems)
                        {
                            if (source.Contains(subItem))
                            {
                                indexes.Add(source.IndexOf(subItem));
                            }
                            else
                            {
                                matches = false;
                                break;
                            }
                        }

                        if (!matches)
                            break;

                        for (int i = indexes.Count - 1; i > 0; i--)
                        {
                            if (indexes[i] - indexes[i - 1] - subItems[i - 1].Length != 1)
                            {
                                matches = false;
                                break;
                            }
                        }
                        if (matches)
                        {
                            output.Add(indexes[0] + differencesSum, new String(source.ToCharArray(), indexes[0], item.Length));
                            source = source.Remove(indexes[0], item.Length);
                            differencesSum += item.Length;
                        }

                    }
                    source = originalSource;
                }
                else
                {
                    if (item[item.Length - 1] == '#')
                    {
                        var newItem = item;
                        newItem = newItem.Remove(newItem.IndexOf('#'));
                        var originalSource = source;
                        while (source.Contains(newItem))
                        {
                            var a = source.IndexOf(newItem);
                            output.Add(source.IndexOf(newItem), newItem);
                            source = source.Remove(source.IndexOf(newItem), newItem.Length);
                        }
                        source = originalSource;
                    }
                    else
                    {
                        while (source.Contains(item))
                        {
                            if (allWords.Contains(item))
                            {
                                output.Add(source.IndexOf(item), item);
                                source = source.Remove(source.IndexOf(item), item.Length);
                            }
                            else
                                break;
                        }

                    }
                }
            }

            return output;

        }

        void BuildTree(List<Token> tokens, string source, string expr)
        {
            //if (!success)
            //    helpa(source, expr);
            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            TreeNode treeNodeAssign = new TreeNode();
            treeNodeAssign.Text = tokens[0].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeAssign);
            treeView.SelectedNode = treeNodeAssign;

            TreeNode treeNodeVariable1 = new TreeNode();
            treeNodeVariable1.Text = tokens[1].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeVariable1);

            TreeNode treeNodeVariable2 = new TreeNode();
            treeNodeVariable2.Text = tokens[3].Value;
            treeView.SelectedNode.Nodes.Add(treeNodeVariable2);
            treeView.SelectedNode = treeView.SelectedNode.Parent;

        }

        private void helpa(string source, string expr)
        {
            while (true)
            {
                var lapki = expr.IndexOf('\"');
                if (expr[0] == '\"' || lapki != -1)
                    expr = expr.Remove(lapki, 1);
                else
                    break;
            }

            var regex = new Regex(expr);
            using (var streamWriter = new StreamWriter(Path, true))
            {
                foreach (Match match in regex.Matches(source))
                {
                    streamWriter.WriteLine($"Found {match.Value} at position {match.Index}");
                }
            }
        }
    }

    public class IfBranch : IStatement
    {
        public List<Token> _tokens { get; }
        public TreeView treeView { get; set; }

        private List<IStatement> _ifStatements;
        private List<IStatement> _elseStatements;
        private Condition _condition;
        public IfBranch(List<Token> tokens, List<IStatement> ifStatements, List<IStatement> elseStatements)
        {
            _tokens = tokens;
            _ifStatements = ifStatements;
            _elseStatements = elseStatements;
            _condition = new Condition(_tokens);
        }

        public void Execute(Dictionary<string, string> variables)
        {
            bool isExecuted = false;

            //BuildTree(_tokens, 1);
            //BuildTree(_tokens, 0);
            if (_condition.Evaluate(variables))
            {
                BuildTree(_tokens, 1);
                foreach (var statement in _ifStatements)
                {
                    statement.treeView = this.treeView;
                    statement.Execute(variables);
                }
                isExecuted = true;
            }
            else
            {
                if (_tokens.Count != 1 && _tokens[0].Value != "0")
                {
                    BuildTree(_tokens, 0);
                    foreach (var statement in _elseStatements)
                    {
                        statement.treeView = this.treeView;
                        statement.Execute(variables);
                    }
                    isExecuted = true;
                }
            }
            if (isExecuted)
            {
                int count = 1;
                while (true)
                {
                    treeView.SelectedNode = treeView.SelectedNode.Parent;
                    if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                            || treeView.SelectedNode.Text == "else")
                    {
                        if (count == 1)
                            break;
                        else
                            count++;
                    }
                }
            }
        }
        void BuildTree(List<Token> tokens, int flag)
        {
            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            var treeNode = new TreeNode();
            if (flag == 1)
                treeNode.Text = "if";
            else
                treeNode.Text = "else";
            treeView.SelectedNode.Nodes.Add(treeNode);
            treeView.SelectedNode = treeNode;

            var treeNodeCondition = new TreeNode();
            treeNodeCondition.Text = "condition:";
            treeView.SelectedNode.Nodes.Add(treeNodeCondition);

            if (tokens.Count == 1)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[0].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
            }
            else if (tokens.Count == 2)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[0].Value + tokens[1].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
            }
            else if (tokens.Count == 3)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[1].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
                treeView.SelectedNode = treeNode1;

                var treeNode0 = new TreeNode();
                treeNode0.Text = tokens[0].Value;
                treeView.SelectedNode.Nodes.Add(treeNode0);

                var treeNode2 = new TreeNode();
                treeNode2.Text = tokens[2].Value;
                treeView.SelectedNode.Nodes.Add(treeNode2);

            }

            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            var treeNodeBody = new TreeNode();
            treeNodeBody.Text = "body:";
            treeView.SelectedNode.Nodes.Add(treeNodeBody);
        }
    }

    public class WhileLoop : IStatement
    {
        public List<Token> _tokens { get; }
        private List<IStatement> _whileStatements;
        private Condition _condition;
        public TreeView treeView { get; set; }

        public WhileLoop(List<Token> tokens, List<IStatement> whileStatements)
        {
            _tokens = tokens;
            _whileStatements = whileStatements;
            _condition = new Condition(_tokens);
        }
        public void Execute(Dictionary<string, string> variables)
        {
            while (_condition.Evaluate(variables))
            {
                BuildTree(_tokens);
                foreach (var statement in _whileStatements)
                {
                    statement.treeView = treeView;
                    statement.Execute(variables);
                }

            }
         //   treeView.SelectedNode = treeView.SelectedNode.Parent;
        }
        void BuildTree(List<Token> tokens)
        {
            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            var treeNode = new TreeNode();
            treeNode.Text = "while";
            treeView.SelectedNode.Nodes.Add(treeNode);
            treeView.SelectedNode = treeNode;

            var treeNodeCondition = new TreeNode();
            treeNodeCondition.Text = "condition:";
            treeView.SelectedNode.Nodes.Add(treeNodeCondition);

            if (tokens.Count == 1)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[0].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
            }
            else if (tokens.Count == 2)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[0].Value + tokens[1].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
            }
            else if (tokens.Count == 3)
            {
                var treeNode1 = new TreeNode();
                treeNode1.Text = tokens[1].Value;
                treeView.SelectedNode.Nodes.Add(treeNode1);
                treeView.SelectedNode = treeNode1;

                var treeNode0 = new TreeNode();
                treeNode0.Text = tokens[0].Value;
                treeView.SelectedNode.Nodes.Add(treeNode0);

                var treeNode2 = new TreeNode();
                treeNode2.Text = tokens[2].Value;
                treeView.SelectedNode.Nodes.Add(treeNode2);

            }

            while (true)
            {
                if (treeView.SelectedNode.Text == "start" || treeView.SelectedNode.Text == "if"
                        || treeView.SelectedNode.Text == "else")
                    break;
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
            var treeNodeBody = new TreeNode();
            treeNodeBody.Text = "body:";
            treeView.SelectedNode.Nodes.Add(treeNodeBody);
        }

    }
}
