﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Interpretator
    {
        private readonly string _code;
        private readonly Lexer _lexer;
        private List<Token> _tokens;
        private Syntaxer _syntaxer;
        public Dictionary<string, string> _variables;
        private TextBox _textBox;
        private TreeView _treeView;
        private List<IStatement> _statements;

        public Interpretator(string code, TextBox textBox, TreeView treeView)
        {
            _code = code;
            _lexer = new Lexer(code);
            _tokens = new List<Token>();
            _variables = new Dictionary<string, string>() { {"a", "5" } };
            _textBox = textBox;
            _treeView = treeView;
            _statements = new List<IStatement>();
        }

        public async Task ExecuteAsync()
        {
            _tokens = _lexer.Execute();
           // await PrintTokensAsync(_tokens);
            _syntaxer = new Syntaxer(_tokens);
            _syntaxer.Execute(_variables, _statements);
            // BuildSyntaxTree();

            TreeNode startNode = new TreeNode();
            startNode.Text = "start";
            _treeView.Nodes.Add(startNode);
            _treeView.SelectedNode = startNode;

            foreach (var statement in _statements)
            {
                statement.treeView = _treeView;
                statement.Execute(_variables);
            }

        }

        private void BuildSyntaxTree()
        {
            TreeNode startNode = new TreeNode();
            startNode.Text = "start";
            _treeView.Nodes.Add(startNode);
            
            foreach (var statement in _statements)
            {
                foreach (var token in statement._tokens)
                {
                    TreeNode treeNode = new TreeNode();
                    treeNode.Text = token.Value;
                    _treeView.Nodes[0].Nodes.Add(treeNode);

                }
            }
            


        }

        private async Task PrintTokensAsync(List<Token> tokens)
        {
            foreach (var token in tokens)
            {
                _textBox.AppendText(token.Value + " - " + token.tokenType.ToString());
                _textBox.AppendText(Environment.NewLine);
            }
        }
    }
}
