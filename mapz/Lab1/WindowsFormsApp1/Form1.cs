﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            textBoxOutput.Clear();
            treeView.Nodes.Clear();
            if(richTextBoxInput.Text != "")
            {
                try
                {
                    var interpretator = new Interpretator(richTextBoxInput.Text, textBoxOutput, treeView);
                    await interpretator.ExecuteAsync();
                    WriteResults();
                }
                catch (Exception ex)
                {
                    textBoxOutput.AppendText("Exception: " + ex.Message);
                }
            }
        }

        private void WriteResults()
        {
            string path = "D:\\1univer\\Semester_4\\MAPZ\\Interpretator\\output.txt";
            try 
            {
                if (File.Exists(path))
                {
                    string result = File.ReadAllText(path);
                    textBoxOutput.AppendText(result);
                    File.WriteAllText(path, string.Empty);
                }
                else 
                    textBoxOutput.AppendText("Exception: Couldn't find file");
            }
            catch
            {
                textBoxOutput.AppendText("Exception: Couldn't open file");
            }  
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.F)
            {
                searchBox.Visible = true;
            }
        }

        private void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Shift)
            {
                int index = 0;
                string temp = richTextBoxInput.Text;
                richTextBoxInput.Text = "";
                richTextBoxInput.Text = temp;

                while (index < richTextBoxInput.Text.LastIndexOf(searchBox.Text))
                {
                    richTextBoxInput.Find(searchBox.Text, index, richTextBoxInput.TextLength, RichTextBoxFinds.None);
                    richTextBoxInput.SelectionBackColor = Color.Yellow;
                    index = richTextBoxInput.Text.IndexOf(searchBox.Text, index) + 1;
                }
            }
        }
    }
}
