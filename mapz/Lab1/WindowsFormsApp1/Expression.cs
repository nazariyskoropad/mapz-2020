﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Expression
    {
        public List<Token> _tokens;

        public Expression(List<Token> tokens)
        {
            _tokens = tokens;
        }
        public string Evaluate(Dictionary<string, string> variables)
        {
            _tokens = InfixToPostfix(variables);
            var stack = new Stack<string>();
            foreach (var token in _tokens)
            {
                if (token.tokenType == TokenType.constant)
                    stack.Push(token.Value);
                else if (token.tokenType == TokenType.varname)
                {
                    var variableToken = variables.SingleOrDefault(v => v.Key == token.Value);
                    if (!variableToken.Equals(default(KeyValuePair<string, string>)))
                        stack.Push(variableToken.Value);
                    else
                        throw new Exception("Variable doesnt exist");
                }
                else
                {
                    if (stack.Count < 2)
                        throw new Exception("Wrong expression");
                    var rightValue = stack.Peek();
                    stack.Pop();
                    var leftValue = stack.Peek();
                    stack.Pop();

                    string tempValue;
                    switch (token.Value)
                    {
                        case "+":
                            {
                                if (IsDigit(leftValue) && IsDigit(rightValue))
                                    tempValue = (Convert.ToInt32(leftValue) + Convert.ToInt32(rightValue)).ToString();
                                else
                                {

                                    var lapki = leftValue.LastIndexOf('\"');
                                    if (leftValue[0] == '\"' || lapki != -1)
                                        leftValue = leftValue.Remove(lapki, 1);
                                    tempValue = leftValue + rightValue;
                                }
                                stack.Push(tempValue);
                                break;
                            }
                        case "-":
                            {
                                if (IsDigit(leftValue) && IsDigit(rightValue))
                                    tempValue = (Convert.ToInt32(leftValue) - Convert.ToInt32(rightValue)).ToString();
                                else
                                    throw new Exception("Invalid string operation");
                                stack.Push(tempValue);
                                break;
                            }
                        case "/":
                            {
                                if (IsDigit(leftValue) && IsDigit(rightValue))
                                {
                                    if (rightValue != "0")
                                        tempValue = (Convert.ToInt32(leftValue) / Convert.ToInt32(rightValue)).ToString();
                                    else throw new Exception("Cant divide by zero");
                                }
                                else
                                    throw new Exception("Invalid string operation");
                                stack.Push(tempValue);
                                break;
                            }
                        case "*":
                            {
                                if (IsDigit(leftValue) && IsDigit(rightValue))
                                {
                                    tempValue = (Convert.ToInt32(leftValue) * Convert.ToInt32(rightValue)).ToString();
                                }
                                else
                                    throw new Exception("Invalid string operation");
                                stack.Push(tempValue);
                                break;
                            }
                        default: throw new Exception("Unknown operator");
                    }
                }
            }
            return stack.Peek();
        }
        private bool IsDigit(string str)
        {
            if (str[0] != '-' && !Char.IsDigit(str[0]))
                return false;
            else
                foreach (var ch in str)
                {
                    if (!Char.IsDigit(ch))
                        return false;
                }
            return true;
        }
        private string CheckExpression()
        {
            string str = "";
            foreach (var token in _tokens)
                str += token.Value;
            var chars = str.ToCharArray();
            Array.Reverse(chars);
            return new String(chars);
        }

        private List<Token> InfixToPostfix(Dictionary<string, string> variables)
        {
            var result = new List<Token>();
            var stack = new Stack<Token>();
            foreach (var token in _tokens)
            {
                if (token.tokenType == TokenType.constant)
                    result.Add(token);
                else if (token.tokenType == TokenType.varname)
                {
                    if (variables.ContainsKey(token.Value))
                        result.Add(token);
                    else
                        throw new Exception("Variable doesnt exist");
                }
                else if (token.tokenType == TokenType.lparent)
                    stack.Push(token);
                else if (token.tokenType == TokenType.rparent)
                {
                    while (stack.Count != 0 && stack.Peek().tokenType != TokenType.lparent)
                    {
                        result.Add(stack.Peek());
                        stack.Pop();
                    }
                    if (stack.Count != 0 && stack.Peek().tokenType != TokenType.lparent)
                    {
                        throw new Exception("Evaluation failed");
                    }
                    stack.Pop();
                }
                else
                {
                    while (stack.Count != 0 && GetWeightOfOperator(token) <= GetWeightOfOperator(stack.Peek()))
                    {
                        result.Add(stack.Peek());
                        stack.Pop();
                    }
                    stack.Push(token);
                }
            }
            while (stack.Count != 0)
            {
                result.Add(stack.Peek());
                stack.Pop();
            }
            return result;
        }
        private bool IsCorrect()
        {
            throw new NotImplementedException();

        }
        private int GetWeightOfOperator(Token token)
        {
            if (token.Value == "*" || token.Value == "/")
                return 3;
            if (token.Value == "+" || token.Value == "-")
                return 2;
            return 1;
        }
    }
}
