﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models.Enums;

namespace WpfApp1.Constants
{
    public static class EnemyConstants
    {
        private static Random _random = new Random();
        public const int RandomMinValue = 0;
        public const int RandomMaxValue = 10;
        public const int HardLevelAdd = -4;
        public const int MidLevelAdd = -6;
        public const int EasyLevelAdd = -8;

        public static int GetValueToAdd(Level level)
        {
            switch (level)
            {
                case Level.Hard:
                    return _random.Next(
                EnemyConstants.RandomMinValue, EnemyConstants.RandomMaxValue) - EnemyConstants.HardLevelAdd;
                case Level.Medium:
                    return _random.Next(
                EnemyConstants.RandomMinValue, EnemyConstants.RandomMaxValue) - EnemyConstants.MidLevelAdd;
                case Level.Easy:
                    return _random.Next(
                EnemyConstants.RandomMinValue, EnemyConstants.RandomMaxValue) - EnemyConstants.EasyLevelAdd;
            }
            return 0;
        }
    }
}
