﻿using System.Collections.Generic;
using System.Windows.Documents;

namespace WpfApp1.Models
{
    public class Footballer : BaseEntity
    {
        public string Name { get; set; }
        public int Level { get; }
        public int Speed { get; }
        public int Shot { get; }
        public int Pass { get; }
        public int Strength { get; }
        public Boots.Boots Boots { get; set; }
        public TShirts.TShirt TShirt { get; set; }
        public List<IBoost> Boosts { get; set; }


    }
}