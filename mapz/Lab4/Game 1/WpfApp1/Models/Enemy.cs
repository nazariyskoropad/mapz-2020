﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Models
{
    public class Enemy
    {
        public int Level { get; set; }
        public int Speed { get; set; }
        public int Shot { get; set; }
        public int Pass { get; set; }
        public int Strength { get; set; }
        public Boots.Boots Boots { get; set; }
        public TShirts.TShirt TShirt { get; set; }
        public List<IBoost> Boosts { get; set; }

        public virtual void MakeShot()
        {
            Console.WriteLine("Making shot");
        }
    }
}
