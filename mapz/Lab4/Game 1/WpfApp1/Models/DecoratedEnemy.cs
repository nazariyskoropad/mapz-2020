﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1.Models
{
    public class DecoratedEnemy : Enemy
    {
        protected Enemy _decoratedEnemy { get; set; }
        public DecoratedEnemy(Enemy decoratedEnemy)
        {
            _decoratedEnemy = decoratedEnemy;
        }
        public override void MakeShot()
        {
            _decoratedEnemy.MakeShot();
        }
    }

    //додаємо новий функціонал методу MakeShot()
    public class TalkingEnemy : DecoratedEnemy
    {
        private ITalker _talker;
        public TalkingEnemy(Enemy decoratedEnemy, ITalker talker) : base(decoratedEnemy)
        {
            _talker = talker;
        }
        public override void MakeShot()
        {
            base.MakeShot();
            _talker.Talk();
        }
    }
}
