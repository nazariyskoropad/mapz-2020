﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Controls;

namespace WpfApp1.Models
{
    public class Fan
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Bitmap Image { get; set; }
    }

    public class SittingFan : Fan
    {
        public SittingFan()
        {
            Image = FanImagesFactory.CrateSittingFanImage();
        }
    }

    public class StandingFan : Fan
    {
        public StandingFan()
        {
            Image = FanImagesFactory.CrateStandingFanImage();
        }
    }

    public class FanImagesFactory
    {
        public static Dictionary<Type, Bitmap> Images = new Dictionary<Type, Bitmap>();
        public static Bitmap CrateSittingFanImage()
        {
            if (!Images.ContainsKey(typeof(SittingFan)))
            {
                Images.Add(typeof(SittingFan), new Bitmap("Sitting_Fan.jpg"));
            }
            return Images[typeof(SittingFan)];
        }
        public static Bitmap CrateStandingFanImage()
        {
            if (!Images.ContainsKey(typeof(StandingFan)))
            {
                Images.Add(typeof(StandingFan), new Bitmap("Standing_Fan.jpg"));
            }
            return Images[typeof(StandingFan)];
        }
    }

}
