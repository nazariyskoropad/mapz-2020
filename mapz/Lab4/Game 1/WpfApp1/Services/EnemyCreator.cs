﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfApp1.Models;

namespace WpfApp1.Services
{
    class EnemyCreator
    {
        private EnemyBuilder _enemyBuilder;
        public void SetEnemyBuilder(EnemyBuilder enemyBuilder)
        {
            _enemyBuilder = enemyBuilder;
        }
        public Enemy GetEnemy()
        {
            return _enemyBuilder.GetEnemy();
        }
        public void ConstructEnemy()
        {
            _enemyBuilder.SetPass();
            _enemyBuilder.SetShot();
            _enemyBuilder.SetSpeed();
            _enemyBuilder.SetStrength();
        }
    }

}
